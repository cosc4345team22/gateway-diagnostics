import os
import sys


#check if psutill is instaleld
if os.system("pip show psutil") != 0:
    #install psutils if not installed on computer
    psutilInstall = "pip install psutil"
    if os.system(psutilInstall) != True:
        sys.stderr.err = "Cannot install psutils"
import psutil

def secs2hours(secs):
    mm, ss = divmod(secs, 60)
    hh, mm = divmod(mm, 60)
    return "%d:%02d:%02d" % (hh, mm, ss)

def main():
    battery = psutil.sensors_battery()

    #stdout
    sys.stdout.write("charge = %s%%, time left = %s" % (battery.percent, secs2hours(battery.secsleft)))


main()