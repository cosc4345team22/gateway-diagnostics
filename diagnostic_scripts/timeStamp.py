import datetime
import sys

#returns time stamp of gatway
def timeStamp():
    timeStamp = datetime.datetime.now()
    timeStampString = timeStamp.strftime("%Y-%m-%d %H:%M:%S")
    return timeStampString

def main():
    sys.stdout.write(timeStamp())

main()