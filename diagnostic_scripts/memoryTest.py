import os
import sys
#check if psutill is instaleld
if os.system("pip show psutil") != 0:
    #install psutils if not installed on computer
    psutilInstall = "pip install psutil"
    if os.system(psutilInstall) != True:
        sys.stderr.err = "Cannot install psutils"
import psutil

def main():
    #memory check
    memoryFullCheck = psutil.disk_usage("/")
    memoryUsed = psutil.disk_usage("/").used
    memoryRemaining = psutil.disk_usage("/").free
    memoryUsedPercent = psutil.disk_usage("/").percent


    #sys.stdout
    sys.stdout.write("Memory Used:"+ str(memoryUsed)+"bytes  --  "+str(memoryUsedPercent)+ "%\n")
    sys.stdout.write("Memory Remaining:"+str(memoryRemaining)+"bytes  --  "+str(100-memoryUsedPercent)+"%\n")

main()