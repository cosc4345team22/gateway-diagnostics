from platform import system as system_name  # Returns the system/OS name
from subprocess import call as system_call  # Execute a shell command
import sys
def ping(host):

    # Ping command count option as function of OS
    param = '-n' if system_name().lower()=='windows' else '-c'

    # Building the command. Ex: "ping -c 1 google.com"
    command = ['ping', param, '1', host]

    # Pinging
    return system_call(command) == 0

def main(host):
    gateway = host
    call = ping(gateway)
    if call == True:
        sys.stdout.write("Gateway "+host+": connected to server.")
    else:
        sys.stderr.write("Gateway not connected to server")
main("www.google.com")