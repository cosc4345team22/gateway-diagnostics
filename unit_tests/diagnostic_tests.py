from diagnostic_scripts import batteryTest
import unittest
import subprocess

class TestDiganostics(unittest.TestCase):
    def testBattery(self):
        proc = subprocess.Popen(['python', '-u', '../diagnostic_scripts/batteryTest.py'], stdout=subprocess.PIPE)
        out, err = proc.communicate()
        self.assertIsNone(err)
    def testMemory(self):
        proc = subprocess.Popen(['python', '-u', '../diagnostic_scripts/memoryTest.py'], stdout=subprocess.PIPE)
        out, err = proc.communicate()
        self.assertIsNone(err)
    def testConnectivity(self):
        proc = subprocess.Popen(['python', '-u', '../diagnostic_scripts/connectivityTest.py'], stdout=subprocess.PIPE)
        out, err = proc.communicate()
        self.assertIsNone(err)
    def testCPU(self):
        proc = subprocess.Popen(['python', '-u', '../diagnostic_scripts/cpuUsageTest.py'], stdout=subprocess.PIPE)
        out, err = proc.communicate()
        self.assertIsNone(err)